import React, {Component} from 'react';
import bts from './img/bts.gif';
import './App.css';

class App extends Component {
  

  constructor() {
    super()
    
    this.state = { count: 0, canVote:true }
    this.btsClicked = this.voteBTS.bind(this)
  
  }

  voteBTS(event) {
    console.log("you voted bts");
    let clicks = this.state.count;
    clicks = clicks + 1;
    this.setState({count:clicks, canVote:true});

    if (this.state.count >= 10) {
      console.log("maximum clicks counted");
      this.setState({canVote:false});
    }
  }

  render() {
    let message;
    if (this.state.canVote === false) {
      message = 
          <div>
              <p>You've reached them maximum! No votes allowed.</p>
          </div>
    }    

    return (
      <div>
        <img src={bts}/> <br/>
        <button onClick={this.btsClicked} disabled={!this.state.canVote}> Vote BTS </button>
        <div> You voted for BTS {this.state.count} times.</div>
        { message }
      </div>
    )
  }
}

export default App;
 